<?php

return[
  'doctrine' => [
    'connection' => [
      'orm_default' => [
        'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
        'params' => [
          'host'     => 'mysql',
          'port'     => '3306',
          'user'     => 'collect',
          'password' => 'collectPass',
          'dbname'   => 'itprospect_collect',
        ]
      ]
    ]
  ]    
];
