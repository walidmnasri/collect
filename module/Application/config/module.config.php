<?php
/**
 * Zend Framework (http://framework.zend.com/]
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c] 2005-2015 Zend Technologies USA Inc. (http://www.zend.com]
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return [
    'doctrine' => [
        'driver' => [
            'application_entities' => [
                'class' =>'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [__DIR__ . '/../src/Application/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    'Application\Entity' => 'application_entities'
                ]
            ]
        ]
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '/app/:token',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action'     => 'index',
                    ],
                    'constraints' => [
                        'token' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            'identify' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/identify/:token',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'identify',
                    ],
                    'constraints' => [
                        'token' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            'subscribe' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/subscribe[/:token][/:id]',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'subscribe',
                        'token' => '[a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ],
                ],
            ],
            'save' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/save[/:token][/:id]',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'save',
                    ],
                    'constraints' => [
                        'token' => '[a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ],
                ],
            ],
            'export' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/export[/:token][/:id]',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Crm',
                        'action'        => 'index',
                    ],
                    'constraints' => [
                        'token' => '[a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ],
                ],
            ],
            'coupon' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/coupon[/:action][/:token][/:id]',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Coupon',
                        'action'        => 'index',
                    ],
                    'constraints' => [
                        'action' => '[a-zA-Z0-9_-]*',
                        'token' => '[a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ],
                ],
            ],
            'survey' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/survey[/:action][/:token][/:id]',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'DeliverySurvey',
                        'action'        => 'index',
                    ],
                    'constraints' => [
                        'action' => '[a-zA-Z0-9_-]*',
                        'token' => '[a-zA-Z0-9_-]*',
                        'id' => '[0-9]*',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            'form-service' => 'Application\Factory\FormServiceFactory',
            'costumer-service' => 'Application\Factory\CostumerServiceFactory',
            'crm-service' => 'Application\Factory\CrmServiceFactory',
        ],
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'aliases' => [
            'translator' => 'MvcTranslator',
        ],
        'invokables' => [
            'qr-code' => 'Application\Service\QrCodeService',
        ],
    ],
    'translator' => [
        'locale' => 'fr_FR',
        'translation_file_patterns' => [
            [
                'type'     => 'phpArray',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            'Application\Controller\Index' => 'Application\Controller\IndexController',
            'Application\Controller\Crm' => 'Application\Controller\CrmController',
            'Application\Controller\Coupon' => 'Application\Controller\CouponController',
            'Application\Controller\DeliverySurvey' => 'Application\Controller\DeliverySurveyController',
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'layout/error'           => __DIR__ . '/../view/layout/error.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    // Placeholder for console routes
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
];
