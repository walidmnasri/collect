<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * Costumer
 *
 * @ORM\Table(name="crm_costumer")
 * @ORM\Entity(repositoryClass="Application\Repository\CostumerRepository")
 */
class Costumer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;


    /**
     * @var string
     *
     * @ORM\Column(name="slogan", type="string", length=100, nullable=true)
     */
    private $slogan;

    /**
     * @var string
     *
     * @ORM\Column(name="big_logo", type="string", length=50, nullable=true)
     */
    private $bigLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="small_logo", type="string", length=50, nullable=true)
     */
    private $smallLogo;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=5000, nullable=true)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_survey", type="string", length=100, nullable=true)
     */
    private $subjectSurvey;

    /**
     * @var string
     *
     * @ORM\Column(name="body_html_survey", type="text", length=5000, nullable=true)
     */
    private $bodyHtmlSurvey;

    /**
     * @var string
     *
     * @ORM\Column(name="body_text_survey", type="text", length=5000, nullable=true)
     */
    private $bodyTextSurvey;

    /**
     * @var string
     *
     * @ORM\Column(name="subject_voucher", type="string", length=100, nullable=true)
     */
    private $subjectVoucher;

    /**
     * @var string
     *
     * @ORM\Column(name="body_html_voucher", type="text", length=5000, nullable=true)
     */
    private $bodyHtmlVoucher;

    /**
     * @var string
     *
     * @ORM\Column(name="body_text_voucher", type="text", length=5000, nullable=true)
     */
    private $bodyTextVoucher;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=45, nullable=false)
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    private $createDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_date", type="datetime", nullable=true)
     */
    private $updateDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Agency", mappedBy="costumer", cascade={"persist", "remove"})
     */
    private $agencies;

    public function __construct()
    {
        $this->agencies = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Costumer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return Costumer
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
    /**
     * Set slogan
     *
     * @param string $slogan
     *
     * @return Costumer
     */
    public function setSlogan($slogan)
    {
        $this->slogan = $slogan;

        return $this;
    }

    /**
     * Get slogan
     *
     * @return string
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Set bigLogo
     *
     * @param string $bigLogo
     *
     * @return Costumer
     */
    public function setBigLogo($bigLogo)
    {
        $this->bigLogo = $bigLogo;

        return $this;
    }

    /**
     * Get bigLogo
     *
     * @return string
     */
    public function getBigLogo()
    {
        return $this->bigLogo;
    }

    /**
     * Set smallLogo
     *
     * @param string $smallLogo
     *
     * @return Costumer
     */
    public function setSmallLogo($smallLogo)
    {
        $this->smallLogo = $smallLogo;

        return $this;
    }

    /**
     * Get smallLogo
     *
     * @return string
     */
    public function getSmallLogo()
    {
        return $this->smallLogo;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return Costumer
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @return string
     */
    public function getSubjectSurvey()
    {
        return $this->subjectSurvey;
    }

    /**
     * @param string $subjectSurvey
     * @return Costumer
     */
    public function setSubjectSurvey($subjectSurvey)
    {
        $this->subjectSurvey = $subjectSurvey;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyHtmlSurvey()
    {
        return $this->bodyHtmlSurvey;
    }

    /**
     * @param string $bodyHtmlSurvey
     * @return Costumer
     */
    public function setBodyHtmlSurvey($bodyHtmlSurvey)
    {
        $this->bodyHtmlSurvey = $bodyHtmlSurvey;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyTextSurvey()
    {
        return $this->bodyTextSurvey;
    }

    /**
     * @param string $bodyTextSurvey
     * @return Costumer
     */
    public function setBodyTextSurvey($bodyTextSurvey)
    {
        $this->bodyTextSurvey = $bodyTextSurvey;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubjectVoucher()
    {
        return $this->subjectVoucher;
    }

    /**
     * @param string $subjectVoucher
     * @return Costumer
     */
    public function setSubjectVoucher($subjectVoucher)
    {
        $this->subjectVoucher = $subjectVoucher;

        return $this;
    }

    /**
     * @return string
     */
    public function getBodyHtmlVoucher()
    {
        return $this->bodyHtmlVoucher;
    }

    /**
     * @param string $bodyHtmlVoucher
     * @return Costumer
     */
    public function setBodyHtmlVoucher($bodyHtmlVoucher)
    {
        $this->bodyHtmlVoucher = $bodyHtmlVoucher;

        return $this;
    }

    /**
     * Get bodyTextVoucher
     *
     * @return string
     */
    public function getBodyTextVoucher()
    {
        return $this->bodyTextVoucher;
    }

    /**
     * Set bodyTextVoucher
     *
     * @param string $bodyTextVoucher
     * @return Costumer
     */
    public function setBodyTextVoucher($bodyTextVoucher)
    {
        $this->bodyTextVoucher = $bodyTextVoucher;

        return $this;
    }



    /**
     * Set token
     *
     * @param string $token
     *
     * @return Costumer
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return Costumer
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     *
     * @return Costumer
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Costumer
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     *  Get agencies
     *
     * @return ArrayCollection
     */
    public function getAgencies()
    {
        return $this->agencies;
    }

    /**
     *  Set agencies
     *
     * @param ArrayCollection $value
     * @return Costumer
     */
    public function setAgencies(ArrayCollection $value)
    {
        $this->agencies = $value;
        return $this;
    }
}
