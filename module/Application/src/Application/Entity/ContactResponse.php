<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="crm_contact_response", indexes={@ORM\Index(name="identity_id", columns={"identity_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ContactResponseRepository")
 */
class ContactResponse
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", nullable=true)
     */
    private $token;

    /**
     * @var integer
     *
     * @ORM\Column(name="voucher_status", type="integer", nullable=false)
     */
    private $voucherStatus = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=false)
     */
    private $createDate;


    /**
     * @var \Application\Entity\ContactIdentity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\ContactIdentity", inversedBy="responses")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="identity_id", referencedColumnName="id")
     * })
     */
    private $identity;

    /**
     * @ORM\OneToMany(targetEntity="Application\Entity\Contact", mappedBy="response", cascade={"persist", "remove"})
     */
    private $contacts;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return ContactResponse
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return ContactResponse
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get Voucher status
     *
     * @return integer
     */
    public function getVoucherStatus()
    {
        return $this->voucherStatus;
    }

    /**
     * Set Voucher status
     *
     * @param integer $voucherStatus
     *
     * @return ContactResponse
     */
    public function setVoucherStatus($voucherStatus)
    {
        $this->voucherStatus = $voucherStatus;

        return $this;
    }


    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set identity
     *
     * @param \Application\Entity\ContactIdentity $identity
     *
     * @return ContactResponse
     */
    public function setIdentity(ContactIdentity $identity = null)
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Get identity
     *
     * @return \Application\Entity\ContactIdentity
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param ArrayCollection $value
     *
     * @return ContactResponse
     */
    public function setContacts(ArrayCollection $value)
    {
        $this->contacts = $value;

        return $this;
    }
}
