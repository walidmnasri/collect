<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Agency;
use Application\Entity\Field;

/**
 * Form
 *
 * @ORM\Table(name="crm_form", indexes={@ORM\Index(name="agency_id", columns={"agency_id"}), @ORM\Index(name="field_id", columns={"field_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\FormRepository")
 */
class Form
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="integer", nullable=false)
     */
    private $required;

    /**
     * @var \Application\Entity\Field
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Field")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var \Application\Entity\Agency
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Agency")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agency_id", referencedColumnName="id")
     * })
     */
    private $agency;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Form
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set required
     *
     * @param integer $required
     *
     * @return Form
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return integer
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set field
     *
     * @param \Application\Entity\Field $field
     *
     * @return Form
     */
    public function setField(Field $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \Application\Entity\Field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set agency
     *
     * @param \Application\Entity\Agency $agency
     *
     * @return Form
     */
    public function setAgency(Agency $agency = null)
    {
        $this->agency = $agency;

        return $this;
    }

    /**
     * Get agency
     *
     * @return \Application\Entity\Agency
     */
    public function getAgency()
    {
        return $this->agency;
    }
}
