<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Modality
 *
 * @ORM\Table(name="crm_modality", indexes={@ORM\Index(name="field_id", columns={"field_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ModalityRepository")
 */
class Modality
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=45, nullable=false)
     */
    private $code;

    /**
     * @var \Application\Entity\Field
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Field", inversedBy="modalities")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Modality
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Modality
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set field
     *
     * @param \Application\Entity\Field $field
     *
     * @return Modality
     */
    public function setField(\Application\Entity\Field $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \Application\Entity\Field
     */
    public function getField()
    {
        return $this->field;
    }
}
