<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contact
 *
 * @ORM\Table(name="crm_contact", indexes={@ORM\Index(name="form_id", columns={"form_id"})})
 * @ORM\Entity(repositoryClass="Application\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100, nullable=true)
     */
    private $value;

    /**
     * @var \Application\Entity\Form
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Form")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="form_id", referencedColumnName="id")
     * })
     */
    private $form;

    /**
     * @var \Application\Entity\ContactIdentity
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\ContactResponse", inversedBy="contacts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="response_id", referencedColumnName="id")
     * })
     */
    private $response;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Contact
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set form
     *
     * @param \Application\Entity\Form $form
     *
     * @return Contact
     */
    public function setForm(Form $form = null)
    {
        $this->form = $form;

        return $this;
    }

    /**
     * Get form
     *
     * @return \Application\Entity\Form
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * Set response
     *
     * @param \Application\Entity\ContactResponse $response
     *
     * @return Contact
     */
    public function setResponse(ContactResponse $response = null)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return \Application\Entity\ContactResponse
     */
    public function getResponse()
    {
        return $this->response;
    }
}
