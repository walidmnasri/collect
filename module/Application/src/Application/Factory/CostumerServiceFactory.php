<?php
namespace Application\factory;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Application\Service\CostumerService;

/**
 * Class FormServiceFactory
 */
class CostumerServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return FormService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $service = new CostumerService();
        /** @var EntityManager $em */
        $em = $serviceLocator->get(EntityManager::class);
        $service->setEntityManager($em);

        return $service;
    }
}
