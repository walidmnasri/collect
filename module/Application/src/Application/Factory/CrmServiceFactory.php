<?php
namespace Application\factory;

use Application\Service\CrmService;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

/**
 * Class FormServiceFactory
 */
class CrmServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return FormService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $phpExcel = new \PHPExcel();
        $service = new CrmService();
        /** @var EntityManager $em */
        $em = $serviceLocator->get(EntityManager::class);
        $service->setEntityManager($em);
        $service->setPhpExcel($phpExcel);

        return $service;
    }
}
