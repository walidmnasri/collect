<?php
namespace Application\factory;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;
use Application\Service\FormService;

/**
 * Class FormServiceFactory
 */
class FormServiceFactory implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @return FormService
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $service = new FormService();
        /** @var EntityManager $em */
        $em = $serviceLocator->get(EntityManager::class);
        $service->setEntityManager($em);

        return $service;
    }
}
