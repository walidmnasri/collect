<?php
namespace Application\Service;

use Application\Entity\Agency;
use Application\Entity\Contact;
use Application\Entity\ContactIdentity;
use Application\Entity\ContactResponse;
use Application\Entity\Costumer;
use Application\Entity\Field;
use Application\Entity\Form;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Http\Request;

class FormService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param $em
     * @return $this
     */
    public function setEntityManager($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param $token
     * @return array
     */
    public function fetchByAgency($token)
    {
        $agency = $this->getAgencyByToken($token);
        if ($agency == null) {
            return;
        }
        return $this->em->getRepository(Form::class)->findBy(['agency' => $agency->getId()]);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return ContactIdentity
     */
    public function saveContact (Request $request, $id = null)
    {
        $identity = $this->getContactIdentity($id);

        // insert data in contactIdentity Table
        $subject = $request->getContent();
        $patternEmail = '/email__[0-9]*/';
        preg_match($patternEmail, $subject, $matchesEmail);
        !isset($matchesEmail[0])?:$identity->setEmail($request->getPost($matchesEmail[0]));
        $patternMobile = '/(phone|mobile)__[0-9]*/';
        preg_match($patternMobile, $subject, $matchesMobile);
        !isset($matchesMobile[0])?:$identity->setMobile($request->getPost($matchesMobile[0]));
        if (!$id) {
            $identity->setCreateDate(new \DateTime());
        } else {
            $identity->setUpdateDate(new \DateTime());
        }

        // insert data in contactResponse Table
        $contactResponse = $this->getContactResponse();
        $responseCollection = new ArrayCollection();
        $contactResponse->setIdentity($identity);
        $contactResponse->setCreateDate(new \DateTime());

        // insert data in contact Table
        $arrayCollection = new ArrayCollection();
        foreach ($request->getPost() as $key => $value) {
            if (count(explode('__', $key)) > 1) {
                $key = explode('__', $key)[1];
            }
            $contact = $this->getContact();
            $contact->setForm($this->getForm($key));
            $contact->setValue($value);
            $contact->setResponse($contactResponse);
            $arrayCollection->add($contact);
        }

        $contactResponse->setContacts($arrayCollection);
        $responseCollection->add($contactResponse);
        $identity->setResponses($responseCollection);

        $this->em->persist($identity);
        $this->em->flush();

        return $identity;
    }

    /**
     * @param Request $request
     * @param $token
     *
     * @return bool|ContactIdentity
     */
    public function login(Request $request, $token)
    {
        $login = $request->getPost('login');
        $patternEmail = '/[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9]+/';
        preg_match($patternEmail, $login, $matchesEmail);

        if (!empty($matchesEmail)) {
            $contact = $this->em->getRepository(ContactIdentity::class)->findOneBy(['email' => $login]);
        } else {
            $contact = $this->em->getRepository(ContactIdentity::class)->findOneBy(['mobile' => $login]);
        }

        if ($contact == null || $contact->getResponses()->current()->getContacts()->current()->getForm()
                ->getAgency()->getCostumer()->getId() != $this->getAgencyByToken($token)->getCostumer()->getId()) {
            return false;
        }

        return $contact;
    }

    /**
     * @param $id
     * @return ContactIdentity
     */
    public function getContactIdentity($id = null)
    {
        return $id ? $this->em->find(ContactIdentity::class, $id) : new ContactIdentity();
    }

    /**
     * @param $id
     * @return ContactResponse
     */
    public function getContactResponse($id = null)
    {
        return $id ? $this->em->find(ContactResponse::class, $id) : new ContactResponse();
    }

    /**
     * @param $id
     * @return Contact
     */
    public function getContact($id = null)
    {
        return $id ? $this->em->find(Contact::class, $id) : new Contact();
    }

    /**
     * @param $id
     * @return Form
     */
    public function getForm($id)
    {
        return $this->em->find(Form::class, $id);
    }

    /**
     * @param $code
     * @return Field
     */
    public function getFieldByCode ($code)
    {
        return $this->em->getRepository(Field::class)->findOneBy(['code' => $code]);
    }

    /**
     * @param $token
     * @return Field
     */
    public function getAgencyByToken ($token)
    {
        return $this->em->getRepository(Agency::class)->findOneBy(['token' => $token]);
    }
}
