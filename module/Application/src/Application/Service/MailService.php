<?php
/**
 * Created by PhpStorm.
 * User: wmnasri
 * Date: 04/10/17
 * Time: 01:29
 */

namespace Application\Service;

use Zend\Mail\Exception\RuntimeException;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

/**
 * Class Mail
 * @package Armenio\Mail
 */
class MailService
{
    /**
     * @param array $config
     * @return bool
     */
    public static function send($config = [])
    {
        $transport = new SmtpTransport(new SmtpOptions([
            'name' => 'pro1.mail.ovh.net',
            'host' => 'pro1.mail.ovh.net',
            'port' => '587',
            'connection_class' => 'login',
            'connection_config' => [
                'username' => 'webmaster@it-prospect.com',
                'password' => 'itprospect@@',
                'ssl' => 'tls',
            ],
        ]));

        $message = new Message();

        $message->setEncoding($config['charset']);

        foreach ($config['from'] as $email => $name) {
            $message->setFrom($email, $name);
            break;
        }

        foreach ($config['to'] as $email => $name) {
            $message->addTo($email, $name);
        }

        if (empty($config['replyTo'])) {
            $config['replyTo'] = $config['from'];
        }

        foreach ($config['replyTo'] as $email => $name) {
            $message->setReplyTo($email, $name);
            break;
        }

        $message->setSubject($config['subject']);

        $htmlBody = '';

        if (!empty($config['html'])) {

            $htmlBody = $config['html'];

        } elseif (!empty($config['template'])) {
            $htmlBody = file_get_contents($config['template']);

            foreach ($config['fields'] as $field => $label) {
                $htmlBody = str_replace(sprintf('{$%s}', $field), $config['post'][$field], $htmlBody);
            }
        } else {
            $maxWidth = 0;
            foreach ($config['fields'] as $label) {
                $currentWidth = mb_strlen($label);
                if ($currentWidth > $maxWidth) {
                    $maxWidth = $currentWidth;
                }
            }

            foreach ($config['fields'] as $label => $filed) {
                $widthDiff = (strlen($filed) - mb_strlen($filed));
                $htmlBody .= sprintf('<strong>%s:</strong> %s', str_pad($label, $maxWidth + $widthDiff, '.', STR_PAD_RIGHT), $filed) . PHP_EOL;
            }

            $htmlBody = '
			<html>
				<body>
					<table>
						<tr>
							<td>
								<div style="font-family: \'courier new\', courier, monospace; font-size: 14px;">' . nl2br($htmlBody) . '</div>
							</td>
						</tr>
					</table>
				</body>
			</html>';
        }

        $html = new MimePart($htmlBody);
        $html->type = 'text/html';

        $body = new MimeMessage();
        $body->setParts([$html]);


        $message->setBody($body);

        try {
            $transport->send($message);
            return true;
        } catch (RuntimeException $e) {
            return false;
        }
    }
}