<?php
namespace Application\Service;

use Application\Entity\Agency;
use Application\Entity\Costumer;
use Doctrine\ORM\EntityManager;

class CostumerService
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function setEntityManager($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param string $token
     * @return Costumer
     */
    public function getCostumerByToken($token)
    {
        return $this->em->getRepository(Costumer::class)->findOneBy(['token' => $token]);
    }


    /**
     * @param string $token
     * @return Agency
     */
    public function getAgencyByToken($token)
    {
        return $this->em->getRepository(Agency::class)->findOneBy(['token' => $token]);
    }

    /**
     * @param int $id
     * @return Costumer
     */
    public function getCostumer($id)
    {
        return $this->em->find(Costumer::class, $id);
    }
}
