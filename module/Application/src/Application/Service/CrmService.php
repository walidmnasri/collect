<?php
namespace Application\Service;

use Application\Entity\Agency;
use Application\Entity\ContactIdentity;
use Application\Entity\ContactResponse;
use Application\Entity\Costumer;
use Doctrine\ORM\EntityManager;
use Zend\Http\Request;

class CrmService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \PHPExcel
     */
    protected $phpExcel;

    /**
     * @param $em
     * @return $this
     */
    public function setEntityManager($em)
    {
        $this->em = $em;
        return $this;
    }

    /**
     * @param $phpExcel
     * @return $this
     */
    public function setPhpExcel($phpExcel)
    {
        $this->phpExcel = $phpExcel;

        return $this;
    }

    /**
     * @param string $token
     * @return Costumer
     */
    public function getCostumerByToken($token)
    {
        return $this->em->getRepository(Costumer::class)->findOneBy(['token' => $token]);
    }

    /**
     * @param string $token
     * @return Agency
     */
    public function getAgencyByToken($token)
    {
        return $this->em->getRepository(Agency::class)->findOneBy(['token' => $token]);
    }

    /**
     * @param int $id
     * @return ContactIdentity
     */
    public function getContact($id)
    {
        return $this->em->find(ContactIdentity::class, $id);
    }

    /**
     * @param int $id
     * @return ContactResponse
     */
    public function getContactResponse($id)
    {
        return $this->em->find(ContactResponse::class, $id);
    }

    /**
     * @param $token
     * @return ContactResponse
     */
    public function getContactResponseByToken($token)
    {
        $response = $this->em->getRepository(ContactResponse::class)->findOneBy(['token' => $token]);

        return $response;
    }

    /**
     * @param Request $request
     * @param string $token
     * @param int $id
     * @param int $studyId
     * @param int $feedbackId
     *
     * @return bool
     */
    public function exportContact(Request $request, $token, $id, $studyId, $feedbackId)
    {
        $subject = $request->getContent();
        $patternEmail = '/email__[0-9]*/';
        preg_match($patternEmail, $subject, $matchesEmail);

        if (isset($matchesEmail[0]) && $request->getPost($matchesEmail[0])) {
            return $this->exportOneByOne($id, $studyId, $feedbackId);
        } else {
            return $this->exportManyToOneData($token, $id);
        }
    }

    /**
     * @param string $file
     * @param int $studyId
     * @param int $feedbackId
     *
     * @return bool
     */
    public function sendFileToStatflow($file, $studyId, $feedbackId)
    {
        $basePath = realpath(dirname(__FILE__) . '/../../../../../public/files/export');
        $destination = realpath(dirname(__FILE__) . '/../../../../../../statflow/public/ftp/'.$studyId.'/'.$feedbackId.'/import');

        return $this->copyFile($file, $basePath, $destination);
    }

    /**
     * @param string $file
     * @param int $studyId
     * @param int $feedbackId
     *
     * @return bool
     */
    public function receiveFileFromStatflow($file, $studyId, $feedbackId)
    {
        $fileName = substr($file, 0, -4) . '__' . $studyId.'__' . $feedbackId . '__export_statflow.csv';

        $basePath = realpath(dirname(__FILE__) . '/../../../../../../statflow/public/ftp/'.$studyId.'/'.$feedbackId.'/export');
        $destination = realpath(dirname(__FILE__) . '/../../../../../public/files/import');

        $this->copyFile($fileName, $basePath, $destination);

        return $fileName;
    }

    /**
     * @param $file
     * @param Costumer $costumer
     * @return bool
     */
    public function sendMailToContacts($file, Costumer $costumer)
    {
        $destination = realpath(dirname(__FILE__) . '/../../../../../public/files/import');
        $objReader = \PHPExcel_IOFactory::createReader('CSV');
        $objReader->setDelimiter(";");
        $this->phpExcel = $objReader->load($destination . '/' . $file);

        $worksheet = $this->phpExcel->getActiveSheet();
        $keys = [];
        $value = [];
        $results = [];
        $i = 0;
        foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
            foreach ($cellIterator as $cell) {
                if ($row->getRowIndex() == 1) {
                    $keys[] = $cell->getValue();
                } else {
                    $value[$i][] = $cell->getValue();
                }
            }

            if ($i > 0 ) {
                $results[] = array_combine($keys, $value[$i]);
            }
            $i++;
        }

        $responseContact = $this->getContactResponse(explode('-', $file)[0]);
        if ($responseContact != null) {
            $linkArray = explode('/', $results[0]['link']);
            $responseContact->setToken(end($linkArray));
            $this->em->persist($responseContact);
            $this->em->flush();
        }

        foreach ($results as $result) {
            $body = str_replace('___LINK___', $result['link'], $costumer->getBodyHtmlSurvey());
            $body = str_replace('___FIRSTNAME___', $result['firstname'], $body);
            $body = str_replace('___LASTNAME___', $result['lastname'], $body);
            $config = [
                'charset' => 'UTF-8',
                'from' => [
                    'webmaster@it-prospect.com' => $costumer->getName(),
                ],
                'to' => [
                    $result['email'] => $result['firstname'] .' '. $result['lastname'],
                ],
                'subject' => $costumer->getSubjectSurvey(),
                'html' => $body
            ];

            MailService::send($config);
        }

        unlink($destination . '/' . $file);

        return true;
    }

    /**
     * @param ContactResponse $response
     * @return ContactResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function disableVoucher(ContactResponse $response)
    {
        $response->setVoucherStatus(1);

        $this->em->persist($response);
        $this->em->flush();

        return $response;
    }

    /**
     * @param $token
     * @param $link
     * @return bool|\Exception
     */
    public function sendVoucherToContacts($token, $link)
    {
        $response = $this->getContactResponseByToken($token);

        if ($response == null) {
            return new \Exception('This token is not register');
        }

        $contact = $response->getIdentity();
        $costumer = $response->getContacts()->current()->getForm()->getAgency()->getCostumer();

        $config = [
            'charset' => 'UTF-8',
            'from' => [
                'webmaster@it-prospect.com' => $costumer->getName()
            ],
            'to' => [
                $contact->getEmail() => $contact->getEmail(),
            ],
            'subject' => $costumer->getSubjectVoucher(),
            'html' => str_replace('___LINK___', $link, $costumer->getBodyHtmlVoucher())
        ];

        return MailService::send($config);
    }

    /**
     * @param $file
     * @return array
     */
    public function uploadCsv($file)
    {
        $targetDir = realpath(dirname(__FILE__) . '/../../../../../public/files/import');
        $targetFile = $targetDir . '/' .  basename($file["exportCsv"]["name"]);
        $result = ['', 0];
        $csvFileType = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

        // Check if file already exists
        if (file_exists($targetFile)) {
            $result = ["Sorry, file already exists.", 1];
        }

        if($csvFileType != "csv") {
            $result = ["Sorry, only csv files are allowed.", 1];
        }

        if ($result[1] == 0) {
            if (move_uploaded_file($file["exportCsv"]["tmp_name"], $targetFile)) {
                $result = ["The file " . basename($file["exportCsv"]["name"]) . " has been uploaded.", 0];
            } else {
                $result = ["Sorry, there was an error uploading your file.", 1];
            }
        }

        return $result;
    }

    /**
     * @param int $id
     * @param int $studyId
     * @param int $feedbackId
     *
     * @return string fileName
     */
    protected function exportOneByOne($id, $studyId, $feedbackId)
    {
        $basePath = realpath(dirname(__FILE__) . '/../../../../../public/files/export');
        $contact = $this->getContact($id);
        $fileName = $contact->getResponses()->last()->getId().'-'.date('Y-m-dH:i:s') . '.csv';

        $this->addHeaderToCsv($contact, $basePath . '/' . $fileName);
        $this->addElementToCsv($contact, 2, $basePath . '/' . $fileName);

        $this->sendFileToStatflow($fileName, $studyId, $feedbackId);

        return $fileName;
    }

    /**
     * @param string $token
     * @param int $id
     *
     * @return bool
     */
    protected function exportManyToOneData($token, $id)
    {
        $basePath = realpath(dirname(__FILE__) . '/../../../../../public/files/export');
        $fileName = $token.'-'.date('Y-m-d') . '.csv';
        $contact = $this->getContact($id);

        if (file_exists($basePath . '/' . $fileName)) {
            $objReader = \PHPExcel_IOFactory::createReader('CSV');
            $objReader->setDelimiter(";");
            $this->phpExcel = $objReader->load($basePath.'/'.$fileName);
            $worksheet = $this->phpExcel->getActiveSheet();
            $row = $worksheet->getHighestRow() + 1;
            $this->addElementToCsv($contact, $row, $basePath . '/' . $fileName);
            return true;
        } else {
            $this->addHeaderToCsv($contact, $basePath . '/' . $fileName);
            $this->addElementToCsv($contact, 2, $basePath . '/' . $fileName);
            return true;
        }

        return false;
    }

    /**
     * @param string $file
     * @param string $basePath
     * @param string $destination
     *
     * @return bool
     */
    protected function copyFile($file, $basePath, $destination)
    {
        if (file_exists($basePath . '/' . $file)) {
            copy($basePath . '/' . $file, $destination. '/' . $file);
            unlink($basePath . '/' . $file);
            return true;
        }

        return false;
    }

    /**
     * // fill data
     *
     * @param ContactIdentity $contact
     * @param int $position
     * @param string $filePath
     */
    protected function addElementToCsv(ContactIdentity $contact, $position, $filePath)
    {
        $response = $contact->getResponses()->last();
        $this->phpExcel->getActiveSheet()->setCellValue('A' . $position, $response->getCreateDate()->format('Y-m-d'));
        $structure = $response->getContacts()->current()->getForm()->getAgency()->getCode();
        $this->phpExcel->getActiveSheet()->setCellValue('B' . $position, $structure);
        $column = 'B';
        foreach ($response->getContacts() as $data) {
            $column++;
            $this->phpExcel->getActiveSheet()->setCellValue($column . $position, $data->getValue());
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($this->phpExcel, 'CSV');
        $objWriter->setDelimiter(";");
        $objWriter->save($filePath);
    }

    /**
     * // fill Header
     *
     * @param ContactIdentity $contact
     * @param string $filePath
     */
    protected function addHeaderToCsv(ContactIdentity $contact, $filePath)
    {
        $response = $contact->getResponses()->last();
        $this->phpExcel->getActiveSheet()->setCellValue('A1', 'date achat');
        $this->phpExcel->getActiveSheet()->setCellValue('B1', 'structure');
        $column = 'B';
        foreach ($response->getContacts() as $data) {
            $column++;
            $this->phpExcel->getActiveSheet()->setCellValue($column . '1', $data->getForm()->getField()->getCode());
        }

        $objWriter = \PHPExcel_IOFactory::createWriter($this->phpExcel, 'CSV');
        $objWriter->setDelimiter(";");
        $objWriter->save($filePath);
    }
}
