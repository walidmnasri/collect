<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    /**
     * Listing Structures for Costumer
     *
     * @return array|\Zend\Http\Response|ViewModel
     */
    public function indexAction()
    {
        $costumerService = $this->getServiceLocator()->get('costumer-service');
        $token = $this->params()->fromRoute('token', null);
        $costumer = $costumerService->getCostumerByToken($token);

        if (!$token || $costumer == null) {
            return $this->notFoundAction();
        }

        return new ViewModel(['costumer' => $costumer]);
    }

    /**
     * Identify using mobile number or email if contact exist
     *
     * @return array|\Zend\Http\Response|ViewModel
     */
    public function identifyAction()
    {
        $costumerService = $this->getServiceLocator()->get('costumer-service');
        $formService = $this->getServiceLocator()->get('form-service');
        $token = $this->params()->fromRoute('token', null);
        $agency = $costumerService->getAgencyByToken($token);

        if (!$token || $agency == null) {
            return $this->notFoundAction();
        }

        $return = ['costumer' => $agency->getCostumer(), 'agency' => $agency, 'error'=> false];

        if ($this->request->isPost()) {
            $contact = $formService->login($this->request, $token);
            if ($contact != null) {
                return $this->redirect()->toRoute('subscribe', [
                        'token' => $token,
                        'id' => $contact->getId()
                    ]
                );
            }
            $return['error'] = "Invalid Phone number or Email.";
        }

        return new ViewModel($return);
    }

    /**
     * Display Form subscribe for new contact
     * Display Form pre-filled for existing contact
     *
     * @return array|\Zend\Http\Response|ViewModel
     */
    public function subscribeAction()
    {
        $costumerService = $this->getServiceLocator()->get('costumer-service');
        $formService = $this->getServiceLocator()->get('form-service');
        $token = $this->params()->fromRoute('token', null);
        $id = (int) $this->params()->fromRoute('id', 0);
        $form = $formService->fetchByAgency($token);

        if (!$token || $form == null) {
            return $this->notFoundAction();
        }

        $agency = $costumerService->getAgencyByToken($token);
        $contact = $formService->getContactIdentity($id);
        $contactItem = [];

        if ($id && $contact !== null) {
            foreach ($contact->getResponses()->last()->getContacts() as $cont) {
                $contactItem[$cont->getForm()->getField()->getId()] = $cont->getValue();
            }
            $contactItem['id'] = $id;
        }

        return new ViewModel([
            'costumer' => $agency->getCostumer(),
            'agency' => $agency,
            'form' => $form,
            'contactItem' => $contactItem
        ]);
    }

    /**
     * Save New or update contact
     * Add new response to csv
     * Send csv input to StatFlow
     * Execute cron import StatFlow
     * Receive CSV export output from StatFlow
     * Send link to survey into E-mail to contact
     *
     * @return array|\Zend\Http\Response|ViewModel
     * @throws \Exception
     */
    public function saveAction()
    {
        $costumerService = $this->getServiceLocator()->get('costumer-service');
        $formService = $this->getServiceLocator()->get('form-service');
        $export = $this->getServiceLocator()->get('crm-service');

        $token = $this->params()->fromRoute('token', null);
        $id = (int) $this->params()->fromRoute('id', null);
        $agency = $costumerService->getAgencyByToken($token);

        if (!$this->request->getContent() || !$token || $agency == null) {
            return $this->notFoundAction();
        }

        $costumer = $agency->getCostumer();
        $config = $this->getServiceLocator()->get('Config');
        $configCostumer = $config['costumers'][$costumer->getCode()];

        if (empty($configCostumer) || !$configCostumer['study'] || !$configCostumer['survey']) {
            $this->layout('layout/error');
            throw new \Exception('No study or survey attached !');
        }

        $contact = $formService->saveContact($this->request, $id);
        $fileName = $export->exportContact(
            $this->request,
            $costumer->getToken(),
            $contact->getId(),
            $configCostumer['study'],
            $configCostumer['survey']
        );
        file_get_contents($config['cron-import'].'study_id/' . $configCostumer['study'] . '/feedback_form_id/' . $configCostumer['survey']);
        $exportFile = $export->receiveFileFromStatflow($fileName, $configCostumer['study'], $configCostumer['survey']);
        $export->sendMailToContacts($exportFile, $costumer);

        return new ViewModel(['costumer' => $costumer, 'agency' => $agency, 'contact' => $contact]);
    }
}
