<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class CouponController extends AbstractActionController
{
    /**
     * Display voucher
     * Print Voucher png
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function indexAction()
    {
        $this->layout('layout/voucher');
        $token = $this->params()->fromRoute('token', null);
        $this->generateAction();
        return new ViewModel(['token'=> $token]);
    }

    /**
     * Recover Contact response
     * Generate qr code
     * Save qr code png in directory
     * Create Voucher using template
     *
     * @return ViewModel
     * @throws \Exception
     */
    public function generateAction()
    {
        $this->layout('layout/voucher');
        $token = $this->params()->fromRoute('token', null);
        $crm = $this->getServiceLocator()->get('crm-service');
        $response = $crm->getContactResponseByToken($token);

        if (!$token || $response == null) {
            $this->layout('layout/error');
            throw new \Exception('Invalid Respondent');
        }

        if ($response->getVoucherStatus()) {
            $this->layout('layout/error');
            throw new \Exception('This voucher has been used');
        }

        $today = new \DateTime();
        $today->add(new \DateInterval('P30D'));
        $expireDate = $today->format('F d, Y');

        $expiry = $response->getCreateDate()->add(new \DateInterval('P30D'));

        if ($expiry > $today) {
            $this->layout('layout/error');
            throw new \Exception('The validity date of this voucher has been expired');
        }

        $uri = $this->getEvent()->getRouter()->getRequestUri();
        $data =  $uri->getScheme() .'://' . $uri->getHost() . ':81/coupon/valid/e/' . $response->getId();

        $qr = $this->getServiceLocator()->get('qr-code');
        $qr->isHttps(); // or $qr->isHttp();
        $qr->setData($data);
        $qr->setDimensions(90, 90);

        file_put_contents($_SERVER['DOCUMENT_ROOT'] ."/assets/pages/img/qr-code/" . $token . ".png" , file_get_contents($qr->getResult()));
        return new ViewModel(['token'=> $token, 'expireDate' => $expireDate]);
    }

    /**
     * Execute by Qr code scanner
     * Looking if Voucher is valid
     * Change status of voucher
     *
     * @return ViewModel
     */
    public function validAction()
    {
        $crm = $this->getServiceLocator()->get('crm-service');
        $id = $this->params()->fromRoute('id', null);
        $response = $crm->getContactResponse($id);
        $status = 1;
        $message = null;
        $today = new \DateTime();
        $expireDate = $response->getCreateDate()->add(new \DateInterval('P30D'));

        if ($expireDate < $today) {
            $message = 'The validity date of this voucher has been expired';
        }

        if ($response->getVoucherStatus()) {
            $message = 'This voucher has been used';
        }

        if (!$message) {
            $crm->disableVoucher($response);
            unlink($_SERVER['DOCUMENT_ROOT'] ."/assets/pages/img/qr-code/" . $response->getToken() . ".png");
            unlink($_SERVER['DOCUMENT_ROOT'] ."/assets/pages/img/vouchers/" . $response->getToken() . ".png");
            $status = 0;
        }

        return new ViewModel(['status' => $status, 'message' => $message]);
    }

    /**
     * Save Voucher png in directory
     */
    public function saveVoucherAction()
    {
        $token = $this->params()->fromRoute('token', null);
        $imageData = base64_decode($this->request->getPost('imgdata'));
        //path where you want to upload image
        $file = $_SERVER['DOCUMENT_ROOT'] . '/assets/pages/img/vouchers/' . $token . '.png';

        file_put_contents($file, $imageData);
        exit;
    }
}
