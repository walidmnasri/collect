<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CrmController extends AbstractActionController
{

    public function indexAction()
    {
        $crm = $this->getServiceLocator()->get('crm-service');
        $token =  $this->params()->fromRoute('token', null);

        $uri = $this->getEvent()->getRouter()->getRequestUri();
        $link =  $uri->getScheme() .'://' . $uri->getHost() . ':81/coupon/index/' . $token;

        $crm->sendVoucherToContacts($token, $link);

        return new ViewModel();
    }
}