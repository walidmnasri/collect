<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class DeliverySurveyController extends AbstractActionController
{

    public function indexAction()
    {
        $costumerService = $this->getServiceLocator()->get('costumer-service');
        $crm = $this->getServiceLocator()->get('crm-service');
        $token =  $this->params()->fromRoute('token', null);
        $costumer = $costumerService->getCostumerByToken($token);
        if (!$token || $costumer == null) {
            return $this->notFoundAction();
        }
        $result = null;
        if ($this->request->isPost()) {
            $result = $crm->uploadCsv($_FILES);
            $crm->sendMailToContacts($_FILES["exportCsv"]["name"], $costumer);
        }

        return new ViewModel(['result' => $result]);
    }
}